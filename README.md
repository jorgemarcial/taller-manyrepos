# Desarrollo de proyecto distribuido en multiples repositorios compartiendo dependencias. #

## Nuestro proyecto se llama SorianEye, debemos crear los siguientes repositorios en github || bitbucket

* sorianeye-deploy
* sorianeye-api
* sorianeye-cms
* sorianeye-model

* quedando resultante por ejemplo en el primer caso {user}/sorianeye-deploy

## Creación del repositorio modelo.

* clonamos sorianeye-model en nuestro workspace.
* añadimos composer.json:

```json
{
    "name": "{user}/sorianeye-model",
    "description": "SorianEye Model",
    "type": "library",
    "license": "MIT",
    "authors": [
        {
            "name": "Gigigo",
            "email":"gigigo@gigigo.com"
        }
    ],
    "autoload": {
        "psr-4": { "Gigigo\\SorianEyeCoreBundle\\": "" }
    },
    "require": {
        "php": ">=5.3.0"
    }
}
```

* commit y push en master.

## Repositorio deploy y creación de aplicaciones API y CMS.

* clonamos repositorio deploy a nuestro workspace
* añadimos submodulos mediante:

```bash
git submodule add git@bitbucket.org:{user}/sorianeye-cms.git
git submodule add git@bitbucket.org:{user}/sorianeye-api.git
```

Creamos una aplicación symfony en api y cms mediante:

```bash
symfony new source
```

## Añadiendo dependencia Model en nuestros proyectos.

* Añadimos nuestra dependencia (repositories y require) tanto en el proyecto de api como en cms.
* Ejecutamos en ambos proyecto. 

```bash
composer require sorianeye-model dev-master
```

## Creación código modelo y actualización API y CMS

* En cualquiera de los dos proyectos (solamente en uno de ellos) bajamos hasta vendor/{user}/sorianeye-model 
* Añadimos el código de nuestras entidades. Puedes encontrarlo [aquí](https://bitbucket.org/jorgemarcial/taller-manyrepos/src/a15e2102dad106bb9db364267e18b9a51c0b12ee/bundle-classes/model/?at=master)
* Añadimos en AppKernel en el proyecto que nos encontremos. 
```
new Gigigo\SorianEyeCoreBundle\GigigoSorianEyeCoreBundle()
```
* Probamos.
```php
./bin/console ser:run localhost:8000
```

* Si todo ha ido bien, bajar hasta vendor/{user}/sorianeye-model commit y push en master.
* Actualizamos dependencia tanto api como cms con composer require.
* Añadimos la clase al kernel al proyecto restante.
* Probamos
```php
./bin/console ser:run localhost:9000
```